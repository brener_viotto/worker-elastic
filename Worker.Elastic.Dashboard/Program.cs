﻿using Nest;
using Serilog;
using System;
using System.Reactive.Linq;
using Worker.Elastic.Dashboard.Model;
using Worker.Elastic.Dashboard.Repository;

namespace Worker.Elastic.Dashboard
{
    class Program
    {
        static void Main(string[] args)
        {

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            var observable = Observable
               .Interval(TimeSpan.FromMinutes(1));
            //primeiro subscriber
            observable.Subscribe((x) =>
            {
                IntegrarDashboard();

            });
            Console.ReadKey();
        }

        public async static void IntegrarDashboard()
        {
            PedidoElastic pedidoElastic = new PedidoElastic();

            Log.Information("Buscando Dados Sql...");
            pedidoElastic.pedidos();
        }
    }
}
