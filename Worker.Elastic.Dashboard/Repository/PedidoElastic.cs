﻿using System;
using System.Data.SqlClient;
using Dapper;
using Nest;
using Worker.Elastic.Dashboard.Model;

namespace Worker.Elastic.Dashboard.Repository
{
    public class PedidoElastic : BaseRepository
    {

        public async void pedidos()
        {

            var settings = new ConnectionSettings(new Uri("http://localhost:9200"))
    .DefaultIndex("pedidos");
            var client = new ElasticClient(settings);

            

            using (var con = new SqlConnection("Server=52.161.0.22;Database=A2W_Dfe_Prod; User ID=sa; Password=$Acesso@01!!"))
            {

                con.Open();

                string query = $@"select * from (
                    select
                    nf.Id as Id, 
                    nf.dataLancamento as Data,
                    e.Id as FilialId,
                    e.nomeFantasia as Filial,
                    pn.Id as ClienteId,
                    pn.nomeRazaoSocial as Cliente,
                    v.Id as VendedorId,
					v.nomeVendedor as Vendedor,
                    p.Id as ProdutoId,
                    p.descricaoProduto as produto,
                    tc.Id as TipoCulturaId,
                    tc.descricao as TipoCultura,
                    gp.Id as GrupoProdutoId,
                    gp.descricao as GrupoProduto,
                    nfi.qtdItemNota as QtdUnitario,
                    nfi.valorUnitNota as ValorUnitario,
                    (nfi.qtdItemNota * nfi.valorUnitNota) as ValorTotal,
                    (nfi.pesoUnidadeMedidaVendaItem * nfi.qtdItemNota) as PesoTotal,
                    nf.statusMovimento as Status
                    from NotaFiscal as nf inner join NotaFiscalItens as nfi on nfi.notaFiscalId = nf.Id 
                    inner join ParceiroNegocio as pn on pn.Id = nf.ParceiroNegocioId
                    inner join dbo.Empresa as e on e.Id = nf.empresaEmitenteId
                    inner join Vendedor as v on v.Id = nf.vendedorPrincipalId
                    inner join Produto as p on nfi.produtoId = p.Id
                    inner join TipoCultura as tc on tc.Id = nfi.tipoCulturaId
                    inner join GrupoProduto as gp on gp.Id = p.grupoProdutoId
                    ) as tbl";

                var list = await con.QueryAsync<PedidoDTO>(query);
                con.Close();

                Console.WriteLine();

                foreach (var item in list)
                {
                    
                    var response = await client.IndexAsync<PedidoDTO>(item, x => x.Index("pedidosDashboard"));
                    Console.WriteLine(response);

                }


            }
        }

    }

}

