﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;




namespace Worker.Elastic.Dashboard.Repository
{
    public class BaseRepository
    {

        public string ConexaoOracle()
        {
            try
            {
                //conexao com o banco 
                string txtPath = Path.Combine(Environment.CurrentDirectory, "baseOracle.ini");
                TextReader tr = new StreamReader(txtPath);
                string caminha_BD = tr.ReadLine();
                tr.Close();
                string st_conexaoArquivo = caminha_BD;
                return st_conexaoArquivo;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string ConexaoSqlServer()
        {
            try
            {
                //conexao com o banco 
                string txtPath = Path.Combine(Environment.CurrentDirectory, "baseSqlServer.ini");
                TextReader tr = new StreamReader(txtPath);
                string caminha_BD = tr.ReadLine();
                tr.Close();
                string st_conexaoArquivo = caminha_BD;
                return st_conexaoArquivo;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
